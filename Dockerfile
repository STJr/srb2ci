# syntax=docker/dockerfile:1

# Debian Bullseye (11) oldstable
# Debian Bookworm (12) stable
# Debian Trixe    (13) testing
# Debian Forky    (14) sid
ARG DEBIANVER="12"

FROM --platform=$BUILDPLATFORM crazymax/osxcross:12.3-debian AS osxcross

# Use non-slim version
FROM --platform=$BUILDPLATFORM debian:${DEBIANVER}

# Who maint this build image
#MAINTAINER Alam Ed Arias <alam@srb2.org> (https://github.com/alama)
LABEL org.opencontainers.image.authors="alam@srb2.org"
LABEL org.opencontainers.image.url="https://git.do.srb2.org/STJr/srb2ci"
LABEL org.opencontainers.image.source="https://git.do.srb2.org/STJr/srb2ci.git"
LABEL org.opencontainers.image.vendor="Sonic Team Jr."
LABEL org.opencontainers.image.title="SRB2 Builder Image"
LABEL org.opencontainers.image.description="Docker image that can used to compile SRB2 projects"
LABEL org.opencontainers.image.base.name="git.do.srb2.org:5050/stjr/srb2ci/srb2ci"

# Copy osxcross binaries
COPY --link --from=osxcross / /
ENV OSXCROSSVER=12.3

# Include outsiders's libraies
COPY assets/ld.so.conf.d/ /etc/ld.so.conf.d/

# Setup docker's environment
ENV \
    DEBIAN_FRONTEND="noninteractive"                                            \
    DEBIAN_PRIORITY="low"                                                       \
    DEBCONF_NONINTERACTIVE_SEEN="true"                                          \
    OSXCROSSVER=${OSXCROSSVER}                                                  \
    OSXCROSS_SDK=/osxcross/SDK/MacOSX${OSXCROSSVER}.sdk                         \
    OSXCROSS_SDK_VERSION=${OSXCROSSVER}                                         \
    OSXCROSS_TARGET_DIR="/osxcross"                                             \
    OSXCROSS_SDK_DIR="/osxcross/SDK/"                                           \
    OSXCROSS_CCTOOLS_PATH=/osxcross/bin                                         \
    MACOSX_DEPLOYMENT_TARGET=${OSXCROSSVER}                                     \
    VCPKG_ROOT=/opt/vcpkg                                                       \
    VCPKG_BINARY_SOURCES="clear;files,/opt/vcpkg.bcache,read;default,readwrite" \
    VCPKG_DOWNLOADS=/opt/vcpkg.downloads                                        \
    VCPKG_OSX_SYSROOT=/osxcross/SDK/MacOSX${OSXCROSSVER}.sdk                    \
    VCPKG_OVERLAY_TRIPLETS=/opt/vcpkg.triplets                                  \
    POWERSHELL_TELEMETRY_OPTOUT=1                                               \
    PATH="/usr/lib/ccache:$PATH:/opt/vcpkg:/osxcross/bin"                       \
    EMSCRIPTEN_ROOT=/usr/share/emscripten

# Setting up APT conf
COPY assets/apt/ /etc/apt/

RUN \
<<EOF
# Installed Debian sources by version codename
 . /etc/os-release

 ln --force --symbolic --target-directory=/etc/apt/sources.list.d/ --verbose /etc/apt/sources.list.extras/${VERSION_CODENAME}-*.sources
EOF

RUN \
 --mount=type=cache,id=cache.apt,target=/var/cache/apt,sharing=locked \
 --mount=type=cache,id=lib.apt,target=/var/lib/apt,sharing=locked \
 --mount=type=tmpfs,target=/tmp \
<<EOF
# Installing toolchain packages
 set -x -e
#dpkg --add-architecture mips64el
#dpkg --add-architecture s390x
#dpkg --add-architecture ppc64el
#dpkg --add-architecture armel
#dpkg --add-architecture armhf
 dpkg --add-architecture arm64
 dpkg --add-architecture i386
 dpkg --add-architecture amd64
 apt-get update
 apt-get install apt-utils
 apt-get install auto-apt-proxy
 apt-get upgrade tzdata
#echo toolchain: armhf
#apt-get install g++-arm-linux-gnueabihf     || true
#echo toolchain: armel
#apt-get install g++-arm-linux-gnueabi       || true
#echo toolchain: ppc64el
#apt-get install g++-powerpc64le-linux-gnu   || true
#echo toolchain: s390x
#apt-get install g++-s390x-linux-gnu         || true
#echo toolchain: mips64el
#apt-get install g++-mips64el-linux-gnuabi64 || true
 echo toolchain: arm64
 apt-get install g++-aarch64-linux-gnu       || true
 echo toolchain: i386
 apt-get install g++-i686-linux-gnu          || true
 echo toolchain: amd64
 apt-get install g++-x86-64-linux-gnu        || true
#echo SDKs: armhf
#apt-get install \
#libcurl4-openssl-dev:armhf \
#libgme-dev:armhf \
#libibus-1.0-dev:armhf \
#libjack-dev:armhf \
#libltdl-dev:armhf \
#libminiupnpc-dev:armhf \
#libopenmpt-dev:armhf \
#libpng-dev:armhf \
#libsdl2-dev:armhf \
#libsdl2-mixer-dev:armhf \
#libvorbis-dev:armhf \
#libvpx-dev:armhf \
#libyuv-dev:armhf
#echo SDKs: armel
#apt-get install \
#libcurl4-openssl-dev:armel \
#libgme-dev:armel \
#libibus-1.0-dev:armel \
#libjack-dev:armel \
#libltdl-dev:armel \
#libminiupnpc-dev:armel \
#libopenmpt-dev:armel \
#libpng-dev:armel \
#libsdl2-dev:armel \
#libsdl2-mixer-dev:armel \
#libvorbis-dev:armel \
#libvpx-dev:armel \
#libyuv-dev:armel
#echo SDKs: ppc64el
#apt-get install \
#libcurl4-openssl-dev:ppc64el \
#libgme-dev:ppc64el \
#libibus-1.0-dev:ppc64el \
#libjack-dev:ppc64el \
#libltdl-dev:ppc64el \
#libminiupnpc-dev:ppc64el \
#libopenmpt-dev:ppc64el \
#libpng-dev:ppc64el \
#libsdl2-dev:ppc64el \
#libsdl2-mixer-dev:ppc64el \
#libvorbis-dev:ppc64el \
#libvpx-dev:ppc64el \
#libyuv-dev:ppc64el
#echo SDKs: s390x
#apt-get install \
#libcurl4-openssl-dev:s390x \
#libgme-dev:s390x \
#libibus-1.0-dev:s390x \
#libjack-dev:s390x \
#libltdl-dev:s390x \
#libminiupnpc-dev:s390x \
#libopenmpt-dev:s390x \
#libpng-dev:s390x \
#libsdl2-dev:s390x \
#libsdl2-mixer-dev:s390x \
#libvorbis-dev:s390x \
#libvpx-dev:s390x \
#libyuv-dev
#echo SDKs: mips64el
#apt-get install \
#libcurl4-openssl-dev:mips64el \
#libgme-dev:mips64el \
#libibus-1.0-dev:mips64el \
#libjack-dev:mips64el \
#libltdl-dev:mips64el \
#libminiupnpc-dev:mips64el \
#libopenmpt-dev:mips64el \
#libpng-dev:mips64el \
#libsdl2-dev:mips64el \
#libsdl2-mixer-dev:mips64el \
#libvorbis-dev:mips64el \
#libvpx-dev:mips64el \
#libyuv-dev:mips64el
 echo SDKs: arm64
 apt-get install \
 libcurl4-openssl-dev:arm64 \
 libgme-dev:arm64 \
 libibus-1.0-dev:arm64 \
 libjack-dev:arm64 \
 libltdl-dev:arm64 \
 libminiupnpc-dev:arm64 \
 libopenmpt-dev:arm64 \
 libpng-dev:arm64 \
 libsdl2-dev:arm64 \
 libsdl2-mixer-dev:arm64 \
 libvorbis-dev:arm64 \
 libvpx-dev:arm64 \
 libyuv-dev:arm64
 echo SDKS: i386
 apt-get install \
 libcurl4-openssl-dev:i386 \
 libgme-dev:i386 \
 libibus-1.0-dev:i386 \
 libjack-dev:i386 \
 libltdl-dev:i386 \
 libminiupnpc-dev:i386 \
 libopenmpt-dev:i386 \
 libpng-dev:i386 \
 libsdl2-dev:i386 \
 libsdl2-mixer-dev:i386 \
 libvorbis-dev:i386 \
 libvpx-dev:i386 \
 libyuv-dev:i386
 echo SDKs: amd64
 apt-get install \
 libcurl4-openssl-dev:amd64 \
 libgme-dev:amd64 \
 libibus-1.0-dev:amd64 \
 libjack-dev:amd64 \
 libltdl-dev:amd64 \
 libminiupnpc-dev:amd64 \
 libopenmpt-dev:amd64 \
 libpng-dev:amd64 \
 libsdl2-dev:amd64 \
 libsdl2-mixer-dev:amd64 \
 libvorbis-dev:amd64 \
 libvpx-dev:amd64 \
 libyuv-dev:amd64
 echo Get cmake from backports
 apt-get -t bookworm-backports install cmake || true
 apt-get install cmake
 echo tools: hosts
 apt-get install g++ clang make git ccache nasm lld ninja-build
 echo tools: MinGW
 apt-get install mingw-w64
 echo Mingw C++ POSIX set
 update-alternatives --set i686-w64-mingw32-gcc /usr/bin/i686-w64-mingw32-gcc-posix
 update-alternatives --set x86_64-w64-mingw32-gcc /usr/bin/x86_64-w64-mingw32-gcc-posix
 update-alternatives --set i686-w64-mingw32-g++ /usr/bin/i686-w64-mingw32-g++-posix
 update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix
 echo toolchain: WebAssembly
 apt-get install emscripten
 echo toolchain: osxcross
 apt-get install \
 bash \
 bzip2 \
 openssl \
 patch \
 wget
 echo enable HTTPS repos
 apt-get install ca-certificates
 apt-get update
 echo toolchain: .NET
 apt-get install \
 ca-certificates-mono \
 mono-devel \
 msbuild \
 nuget
 echo toolchain: vcpkg
 apt-get install \
 autoconf \
 autopoint \
 bison \
 curl \
 jq \
 libssl-dev \
 libtool \
 libunwind-dev \
 python3-jinja2 \
 tar \
 unzip \
 zip
 echo .NET runtime
 apt-get install dotnet-runtime-9.0
 echo PowerShell TLS
 apt-get install powershell-lts || true
 echo UPGRADE
 apt-get upgrade
 echo CLEANUP
 rm -rf \
 /var/cache/debconf/* \
 /var/lib/dpkg/status-old \
 /var/log/alternatives.log \
 /var/log/apt/eipp.log.xz \
 /var/log/apt/history.log \
 /var/log/apt/term.log \
 /var/log/dpkg.log \
 /var/log/fontconfig.log
EOF

RUN \
 --mount=type=tmpfs,target=/tmp \
<<EOF
 #install powershell if needed
 set -x -e

 pwsh --version && exit 0

 dpa=$(dpkg --print-architecture)
 case "$dpa" in
  amd64)
   export PSARCH=x64;;
  arm64)
   export PSARCH=arm64;;
  armhf)
   export PSARCH=arm32;;
  *)
   export PSARCH=x64-fxdependent;;
 esac

 curl -sL https://api.github.com/repos/PowerShell/PowerShell/releases/latest > /tmp/PowerShell.releases
 cat /tmp/PowerShell.releases | jq -r ".assets[].browser_download_url" | grep "linux-${PSARCH}.tar.gz" > /tmp/PowerShell.URL

 wget `cat /tmp/PowerShell.URL` --no-verbose --output-document=/tmp/powershell-linux.tar.gz

 mkdir --verbose /opt/powershell

 tar -xf /tmp/powershell-linux.tar.gz -C /opt/powershell
 ln --force --symbolic --target-directory=/usr/local/bin --verbose /opt/powershell/pwsh
 chmod +x /opt/powershell/pwsh

 pwsh --version

EOF

RUN <<EOF
#Install vcpkg
 set -x -e
 git clone --quiet --separate-git-dir=/opt/vcpkg.git https://github.com/microsoft/vcpkg.git /opt/vcpkg
 git -C /opt/vcpkg reset --hard `git -C /opt/vcpkg tag --list | tail -n 1`

 mkdir --parents --verbose /opt/vcpkg.bcache $VCPKG_DOWNLOADS

 bootstrap-vcpkg.sh -disableMetrics
EOF

#Copy vcpkg Response Files
COPY assets/vcpkg.res/ /opt/vcpkg.res/

#Copy vcpkg build Files
COPY assets/vcpkg.builds/ /opt/vcpkg.builds/

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages for host system
 set -x -e

#exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/base/*

 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of WASM for SRB2
 set -x -e

#exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo WASM builds for SRB2:

 VCPKG_DEFAULT_TRIPLET=wasm32-emscripten \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res \
 --keep-going || true
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of WASM for RingRacers
 set -x -e

#exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo WASM builds for RingRacer:

 VCPKG_DEFAULT_TRIPLET=wasm32-emscripten \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res \
 --keep-going || true
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Windows x86 for SRB2
 set -x -e

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo Mingw32 x86 builds for SRB2:

 VCPKG_DEFAULT_TRIPLET=x86-mingw-static \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Windows x86 for RingRacers
 set -x -e

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo Mingw32 x86 builds for RingRacers:

 VCPKG_DEFAULT_TRIPLET=x86-mingw-static \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Windows x64 for SRB2
 set -x -e

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo Mingw32 x64 builds for SRB2:

 VCPKG_DEFAULT_TRIPLET=x64-mingw-static \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Windows x64 for RingRacers
 set -x -e

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo Mingw32 x64 builds for RingRacers:

 VCPKG_DEFAULT_TRIPLET=x64-mingw-static \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

# Setting up macport conf
COPY assets/macports/ /osxcross/macports/

RUN \
 --mount=type=cache,id=cache.macports,target=/osxcross/macports/cache,sharing=locked \
 --mount=type=tmpfs,target=/osxcross/macports/tmp \
<<EOF
# Installing packages from macports
 set -x -e
 patch --directory=/osxcross/bin --input=/osxcross/macports/osxcross-macports.arm64-x86_64.patch
 osxcross-macports fakeinstall curl-ca-bundle
 osxcross-macports install --universal64 --static libiconv
 osxcross-macports install --universal64 --static lz4
 osxcross-macports install --universal64 --static gettext-runtime
 osxcross-macports install --universal64 --static xz
 osxcross-macports install --universal64 --static zlib
 osxcross-macports install --universal64 --static openssl3
 osxcross-macports install --universal64 --static brotli
 osxcross-macports install --universal64 --static libunistring
 osxcross-macports install --universal64 --static libidn2
 osxcross-macports install --universal64 --static libpsl
 osxcross-macports install --universal64 --static nghttp2
 osxcross-macports install --universal64 --static openssl
 osxcross-macports install --universal64 --static zstd
 osxcross-macports install --universal64 --static ncurses
 osxcross-macports install --universal64 --static curl
 osxcross-macports install --universal64 --static libpng
 osxcross-macports install --universal64 --static libsdl2
 osxcross-macports install --universal64 --static libjpeg-turbo
 osxcross-macports install --universal64 --static lame
 osxcross-macports install --universal64 --static libmodplug
 osxcross-macports install --universal64          libogg
 osxcross-macports install --universal64          libvorbis
 osxcross-macports install --universal64 --static miniupnpc     || true
 osxcross-macports install --universal64 --static libyuv        || true
 osxcross-macports install --universal64 --static portaudio     || true
 osxcross-macports install --universal64 --static libopenmpt    || true
 osxcross-macports install --universal64 --static mpg123        || true
 osxcross-macports install --universal64 --static libopus       || true
 osxcross-macports install --universal64 --static flac          || true
 osxcross-macports install --universal64 --static libsndfile    || true
 osxcross-macports install --universal64 --static libxmp        || true
 osxcross-macports install --universal64 --static libsdl2_mixer || true

#  Create osxcross bin for non-cmake build systems
 mkdir --parents --verbose /opt/local

 ln -s /osxcross/macports/pkgs/opt/local/libexec /opt/local/
EOF

# vcpkg CMake toolchain files
COPY assets/vcpkg.toolchains/ /opt/vcpkg.toolchains

# Copy custom triplets
COPY assets/vcpkg.triplets /opt/vcpkg.triplets

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Linux amd64 for SRB2
 set -x -e

 exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo Debian:amd64 builds for SRB2:

 CC=x86_64-linux-gnu-gcc \
 CXX=x86_64-linux-gnu-g++ \
 VCPKG_DEFAULT_TRIPLET=x64-linux \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Linux amd64 for RingRacers
 set -x -e

 exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo Debian:amd64 builds for RingRacers:

 CC=x86_64-linux-gnu-gcc \
 CXX=x86_64-linux-gnu-g++ \
 VCPKG_DEFAULT_TRIPLET=x64-linux \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Linux i386 for SRB2
 set -x -e

 exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo Debian:i386 builds for SRB2:

 CC=i686-linux-gnu-gcc \
 CXX=i686-linux-gnu-g++ \
 VCPKG_DEFAULT_TRIPLET=x86-linux \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Linux i386 for RingRacers
 set -x -e

 exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo Debian:i386 builds for RingRacers:

 CC=i686-linux-gnu-gcc \
 CXX=i686-linux-gnu-g++ \
 VCPKG_DEFAULT_TRIPLET=x86-linux \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Linux arm64 for SRB2
 set -x -e

 exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo Debian:arm64 builds for SRB2:
 CC=aarch64-linux-gnu-gcc \
 CXX=aarch64-linux-gnu-g++ \
 VCPKG_DEFAULT_TRIPLET=arm64-linux \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Linux arm64 for RingRacers
 set -x -e

 exit 0

 cd /tmp

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo Debian:arm64 builds for RingRacers:
 CC=aarch64-linux-gnu-gcc \
 CXX=aarch64-linux-gnu-g++ \
 VCPKG_DEFAULT_TRIPLET=arm64-linux \
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
<<EOF
#  Create osxcross bin for non-cmake build systems
 mkdir --parents --verbose /opt/osxcross.x86_64

 ln -s /osxcross/bin/x86_64-*-ar                /opt/osxcross.x86_64/ar
#ln -s /osxcross/bin/x86_64-*-as                /opt/osxcross.x86_64/as
#ln -s /osxcross/bin/x86_64-*-c++               /opt/osxcross.x86_64/c++
#ln -s /osxcross/bin/x86_64-*-cc                /opt/osxcross.x86_64/cc
#ln -s /osxcross/bin/x86_64-*-clang             /opt/osxcross.x86_64/clang
#ln -s /osxcross/bin/x86_64-*-clang++           /opt/osxcross.x86_64/clang++
#ln -s /osxcross/bin/x86_64-*-cmake             /opt/osxcross.x86_64/cmake
 ln -s /osxcross/bin/x86_64-*-dsymutil          /opt/osxcross.x86_64/dsymutil
 ln -s /osxcross/bin/x86_64-*-install_name_tool /opt/osxcross.x86_64/install_name_tool
 ln -s /osxcross/bin/x86_64-*-nm                /opt/osxcross.x86_64/nm
#ln -s /osxcross/bin/x86_64-*-ld                /opt/osxcross.x86_64/ld
 ln -s /osxcross/bin/x86_64-*-osxcross-cmp      /opt/osxcross.x86_64/osxcross-cmp
 ln -s /osxcross/bin/x86_64-*-osxcross-conf     /opt/osxcross.x86_64/osxcross-conf
 ln -s /osxcross/bin/x86_64-*-osxcross-env      /opt/osxcross.x86_64/osxcross-env
 ln -s /osxcross/bin/x86_64-*-osxcross-man      /opt/osxcross.x86_64/osxcross-man
 ln -s /osxcross/bin/x86_64-*-otool             /opt/osxcross.x86_64/otool
#ln -s /osxcross/bin/x86_64-*-pkg-config        /opt/osxcross.x86_64/pkg-config
 ln -s /osxcross/bin/x86_64-*-ranlib            /opt/osxcross.x86_64/ranlib
 ln -s /osxcross/bin/x86_64-*-size              /opt/osxcross.x86_64/size
 ln -s /osxcross/bin/x86_64-*-strings           /opt/osxcross.x86_64/strings
 ln -s /osxcross/bin/x86_64-*-strip             /opt/osxcross.x86_64/strip

 mkdir --parents --verbose /opt/osxcross.arm64

 ln -s /osxcross/bin/arm64-*-ar                 /opt/osxcross.arm64/ar
#ln -s /osxcross/bin/arm64-*-as                 /opt/osxcross.arm64/as
#ln -s /osxcross/bin/arm64-*-c++                /opt/osxcross.arm64/c++
#ln -s /osxcross/bin/arm64-*-cc                 /opt/osxcross.arm64/cc
#ln -s /osxcross/bin/arm64-*-clang              /opt/osxcross.arm64/clang
#ln -s /osxcross/bin/arm64-*-clang++            /opt/osxcross.arm64/clang++
#ln -s /osxcross/bin/arm64-*-cmake              /opt/osxcross.arm64/cmake
 ln -s /osxcross/bin/arm64-*-dsymutil           /opt/osxcross.arm64/dsymutil
 ln -s /osxcross/bin/arm64-*-install_name_tool  /opt/osxcross.arm64/install_name_tool
 ln -s /osxcross/bin/arm64-*-nm                 /opt/osxcross.arm64/nm
#ln -s /osxcross/bin/arm64-*-ld                 /opt/osxcross.arm64/ld
 ln -s /osxcross/bin/arm64-*-osxcross-cmp       /opt/osxcross.arm64/osxcross-cmp
 ln -s /osxcross/bin/arm64-*-osxcross-conf      /opt/osxcross.arm64/osxcross-conf
 ln -s /osxcross/bin/arm64-*-osxcross-env       /opt/osxcross.arm64/osxcross-env
 ln -s /osxcross/bin/arm64-*-osxcross-man       /opt/osxcross.arm64/osxcross-man
 ln -s /osxcross/bin/arm64-*-otool              /opt/osxcross.arm64/otool
#ln -s /osxcross/bin/arm64-*-pkg-config         /opt/osxcross.arm64/pkg-config
 ln -s /osxcross/bin/arm64-*-ranlib             /opt/osxcross.arm64/ranlib
 ln -s /osxcross/bin/arm64-*-size               /opt/osxcross.arm64/size
 ln -s /osxcross/bin/arm64-*-strings            /opt/osxcross.arm64/strings
 ln -s /osxcross/bin/arm64-*-strip              /opt/osxcross.arm64/strip
EOF

# Copy extra SDKs
COPY assets/osxsdk/ /osxsdk/

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Mac x86_64 for SRB2
 set -x -e

 exit 0

 cd /tmp

 export PATH="/opt/osxcross.x86_64:${PATH}"
 `osxcross-conf`
 export OSXCROSS_HOST=x86_64-apple-${OSXCROSS_TARGET}
 export VCPKG_DEFAULT_TRIPLET=x64-osx
 export SDKROOT=${OSXCROSS_SDK}

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo OSX on amd64:
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Mac x86_64 for RingRacers
 set -x -e

 exit 0

 cd /tmp

 export PATH="/opt/osxcross.x86_64:${PATH}"
 `osxcross-conf`
 export OSXCROSS_HOST=x86_64-apple-${OSXCROSS_TARGET}
 export VCPKG_DEFAULT_TRIPLET=x64-osx
 export SDKROOT=${OSXCROSS_SDK}

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo OSX on amd64:
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Mac arm64 for SRB2
 set -x -e

 exit 0

 cd /tmp

 export PATH="/opt/osxcross.arm64:${PATH}"
 `osxcross-conf`
 export OSXCROSS_HOST=arm64-apple-${OSXCROSS_TARGET}
 export VCPKG_DEFAULT_TRIPLET=arm64-osx
 export SDKROOT=${OSXCROSS_SDK}

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/srb2/*

 echo OSX on amd64:
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

RUN \
 --mount=type=cache,id=cache.vcpkg,target=/opt/vcpkg/buildtrees,sharing=locked \
 --mount=type=cache,id=cache.root,target=/root/.cache,sharing=locked \
 --mount=type=tmpfs,target=/opt/vcpkg/packages \
 --mount=type=tmpfs,target=/tmp \
<<EOF
#Build vcpkg's packages of Mac arm64 for RingRacers
 set -x -e

 exit 0

 cd /tmp

 export PATH="/opt/osxcross.arm64:${PATH}"
 `osxcross-conf`
 export OSXCROSS_HOST=arm64-apple-${OSXCROSS_TARGET}
 export VCPKG_DEFAULT_TRIPLET=arm64-osx
 export SDKROOT=${OSXCROSS_SDK}

 ln --force --symbolic --target-directory=/tmp --verbose /opt/vcpkg.builds/ringracers/*

 echo OSX on amd64:
 vcpkg install @/opt/vcpkg.res/vcpkg_install.res
EOF

CMD ["/bin/bash"]
WORKDIR /workdir
