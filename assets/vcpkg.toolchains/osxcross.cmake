# OSXCross toolchain

macro(osxcross_getconf VAR)
  if(NOT ${VAR})
    set(${VAR} "$ENV{${VAR}}")
    if(${VAR})
      set(${VAR} "${${VAR}}" CACHE STRING "${VAR}")
      message(STATUS "Found ${VAR}: ${${VAR}}")
    else()
      message(FATAL_ERROR "Cannot determine \"${VAR}\"")
    endif()
  endif()
endmacro()

osxcross_getconf(VCPKG_ROOT)

find_program(CMAKE_C_COMPILER "$ENV{OSXCROSS_HOST}-clang" PATHS "$ENV{OSXCROSS_TARGET_DIR}/bin" REQUIRED)
find_program(CMAKE_CXX_COMPILER "$ENV{OSXCROSS_HOST}-clang++"  PATHS "$ENV{OSXCROSS_TARGET_DIR}/bin" REQUIRED)

find_program(CMAKE_AR "$ENV{OSXCROSS_HOST}-ar" PATHS "$ENV{OSXCROSS_TARGET_DIR}/bin" REQUIRED)
find_program(CMAKE_RANLIB "$ENV{OSXCROSS_HOST}-ranlib"  PATHS "$ENV{OSXCROSS_TARGET_DIR}/bin" REQUIRED)
find_program(CMAKE_INSTALL_NAME_TOOL "$ENV{OSXCROSS_HOST}-install_name_tool" PATHS "$ENV{OSXCROSS_TARGET_DIR}/bin" REQUIRED)

# where is the target environment
set(CMAKE_FIND_ROOT_PATH "$ENV{OSXCROSS_SDK}")

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

include($ENV{VCPKG_ROOT}/scripts/toolchains/osx.cmake)
